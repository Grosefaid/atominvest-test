Rails.application.routes.draw do
  root 'home#index'
  get '/admin', to: 'admin#index'
  post '/admin/create_rate', to: 'admin#create_rate'
  resources :sessions, only: [:create, :new]
end
