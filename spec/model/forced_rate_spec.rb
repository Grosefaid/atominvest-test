require 'rails_helper'

RSpec.describe ForcedRate, type: :model do
  describe 'validations' do
    subject { described_class.new }

    it 'is valid with valid attributes' do
      subject.rate = 60
      subject.expires = Time.now
      expect(subject).to be_valid
    end

    it 'is not valid without a rate' do
      subject.expires = Time.now
      expect(subject).to_not be_valid
    end

    it 'is not valid without a date' do
      subject.rate = 60
      expect(subject).to_not be_valid
    end

    it 'is not valid with rate < 0' do
      subject.rate = -60
      subject.expires = Time.now
      expect(subject).to_not be_valid
    end
  end
end