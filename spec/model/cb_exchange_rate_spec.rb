require 'rails_helper'

RSpec.describe CbExchangeRate, type: :model do
  describe 'validations' do
    subject { described_class.new }

    it 'is valid with valid attributes' do
      subject.rate = 60
      subject.date = Date.today
      expect(subject).to be_valid
    end

    it 'is not valid without a rate' do
      subject.date = Date.today
      expect(subject).to_not be_valid
    end

    it 'is not valid without a date' do
      subject.rate = 60
      expect(subject).to_not be_valid
    end

    it 'is not valid with rate < 0' do
      subject.rate = -60
      subject.date = Date.today
      expect(subject).to_not be_valid
    end
  end

  describe 'self.stub_for_dev' do
    it 'creates new record' do
      expect {described_class.stub_for_dev}.to change(CbExchangeRate, :count).by(1)
    end

    it 'creates a new record with rate = 60' do
      described_class.stub_for_dev
      expect(CbExchangeRate.last.rate).to eq(65)
    end

    it 'creates a new record with date = Date.today' do
      described_class.stub_for_dev
      expect(CbExchangeRate.last.date).to eq(Date.today)
    end
  end
end