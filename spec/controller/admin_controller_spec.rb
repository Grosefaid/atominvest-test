require 'rails_helper'

RSpec.describe AdminController, type: :controller do
  describe 'GET index' do
    context 'authentication_required!' do
      it 'redirects to session/new' do
        get :index
        expect(response).to redirect_to('/sessions/new')
      end

      it 'renders the new template' do
        get :index, session: {current_user: true}
        expect(response).to render_template('index')
      end
    end
  end

  describe 'POST create_rate' do
    context 'authentication_required!' do
      it 'redirects to session/new' do
        post :create_rate, xhr: true
        expect(response).to redirect_to('/sessions/new')
      end

      it 'renders the new template' do
        post :create_rate, session: {current_user: true}, params: {forced_rate: {rate: 60, expires: Time.now}}, xhr: true
        expect(response).to render_template('create_rate')
      end
    end

    it 'broadcasts message' do
      expect(ActionCable).to receive_message_chain('server.broadcast').with('web_notifications_channel', {rate: 60.to_d})
      post :create_rate, session: {current_user: true}, params: {forced_rate: {rate: 60, expires: 1.minute.from_now}}, xhr: true
    end
  end
end