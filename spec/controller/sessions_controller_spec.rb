require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe 'GET new' do
    it 'renders the new template' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe 'GET create' do
    context 'login is good' do
      before do
        allow_any_instance_of(SiteUser).to receive(:login_valid?).and_return(true)
        get :create, params: {site_user: {username: 'username', password: 'password'}}
      end

      it 'redirects to /admin' do
        expect(response).to redirect_to('/admin')
      end

      it 'updates session' do
        expect(session[:current_user]).to be_truthy
      end
    end

    context 'login is bad' do
      before do
        allow_any_instance_of(SiteUser).to receive(:login_valid?).and_return(false)
        get :create, params: {site_user: {username: 'username', password: 'password'}}
      end

      it 'redirects to /admin' do
        expect(response).to render_template('new')
      end

      it 'updates session' do
        expect(flash[:notice]).to eq('Sorry, wrong credentils')
      end
    end
  end
end