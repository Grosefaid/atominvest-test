class CreateForcedRate < ActiveRecord::Migration[5.2]
  def change
    create_table :forced_rates do |t|
      t.decimal :rate, null: false
      t.datetime :expires, null: false
      t.timestamps
    end
  end
end
