class CreateCbExchangeRate < ActiveRecord::Migration[5.2]
  def change
    create_table :cb_exchange_rates do |t|
      t.decimal :rate, precision: 10, scale: 6, null: false
      t.date :date, null: false
      t.timestamps
    end
  end
end
