class BroadcastWorker
  include Sidekiq::Worker

  def perform
    rate = CbExchangeRate.get_current_rate
    ActionCable.server.broadcast 'web_notifications_channel', rate: rate.rate
  end
end