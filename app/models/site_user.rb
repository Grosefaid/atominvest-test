require 'digest/md5'
class SiteUser
  include ActiveModel::Model

  attr_accessor :username, :password

  def login_valid?
    username == Rails.application.credentials[:login] && Digest::MD5.hexdigest(password) == Rails.application.credentials[:password]
  end
end