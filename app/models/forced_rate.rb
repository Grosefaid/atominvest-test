class ForcedRate < ApplicationRecord
  validates_presence_of :expires, :rate
  validates_numericality_of :rate, greater_than: 0

  after_create :create_broadcast_job

  def create_broadcast_job
    BroadcastWorker.perform_at(self.expires)
  end
end
