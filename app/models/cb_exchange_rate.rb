class CbExchangeRate < ApplicationRecord

  validates_presence_of :date, :rate
  validates_numericality_of :rate, greater_than: 0
  after_create :create_broadcast_job

  def self.stub_for_dev
    CbExchangeRate.create(date: Date.today, rate: 65)
  end

  def self.get_current_rate
    rate = ForcedRate.where('expires > ?', Time.now).last
    rate ||= CbExchangeRate.where(date: Date.today).last
    rate
  end

  def create_broadcast_job
    BroadcastWorker.perform_async
  end

end
