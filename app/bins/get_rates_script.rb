# Скачиваем курсы валют с сайта ЦБР
class GetRatesScript < BinScript
  def do!
    date = (Date.today + 1.day)

    if CbExchangeRate.find_by(date: date).present?
      info "Already exist rate for date #{date}!"
      return
    end

    rate = nil
    3.times do
      begin
        rate = read_rates(date)
        break
      rescue
        sleep(3) unless Rails.env == 'test'
      end
    end

    if rate.nil?
      info "Nil rate for date #{date}"
      return
    else
      info "Creating rate for date #{date} rate #{rate}!"
      CbExchangeRate.create(date: date, rate: rate.to_f)
    end
  end

  def read_rates(date)
    info "Get cbr.ru rates for #{date}..."
    url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=#{date.strftime('%d/%m/%Y')}"
    xml_data = curl_request(url)
    info "Get response: #{xml_data.length} bytes"
    results = Hash.from_xml(xml_data)['ValCurs']['Valute'].inject({}) do |result, elem|
      result[elem['CharCode']] = elem['Value']
      result
    end
    results['USD'].sub(',', '.')
  end

  def curl_request(url)
    `curl -L -A "Mozilla/5.0 (Macintosh; Intel M 10_9_5) AppleWebKit/601.7.8 (KHTML, like Gecko) Version/9.1.3 Safari/537.86.7" '#{url}'`
  end
end
