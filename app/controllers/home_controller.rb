class HomeController < ApplicationController
  def index
    @rate = CbExchangeRate.get_current_rate
  end
end
