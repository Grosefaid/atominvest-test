class AdminController < ApplicationController
  before_action :authentication_required!

  def index
    @rate = ForcedRate.new
    @rates = ForcedRate.order(id: :desc).first(50)
  end

  def create_rate
    @rate = ForcedRate.create(forced_rate_params)
    # should be async, but whatever this is just a test app
    ActionCable.server.broadcast 'web_notifications_channel', rate: @rate.rate if @rate.expires > Time.now
  end

  private

  def forced_rate_params
    params.require(:forced_rate).require(:rate)
    params.require(:forced_rate).require(:expires)
    params.require(:forced_rate).permit(:rate, :expires)
  end
end
